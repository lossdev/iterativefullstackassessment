#!/usr/bin/env bash

if ! command -v docker &> /dev/null; then
    echo "'docker' not found - install docker first!"
    exit
fi

if ! command -v minikube &> /dev/null; then
    echo "'minikube' not found - install minikube first!"
    exit
fi

echo ">> enabling minikube addons ..."
minikube addons enable storage-provisioner
minikube addons enable default-storageclass
minikube addons enable ingress

echo ">> pulling postgres:13 ..."
docker pull postgres:13

echo">> starting minikube cluster ..."
minikube start --driver=docker
echo ">> all done!"
