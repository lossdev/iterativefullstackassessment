#!/usr/bin/env bash

if ! command -v docker &> /dev/null; then
    echo "'docker' not found - install docker first!"
    exit
fi

echo ">> building backend image ..."
cd backend; docker build -t $(cat IMAGE) .

echo ">> building frontend image ..."
cd ../frontend; docker build -t $(cat IMAGE) .

echo ">> all done!"
