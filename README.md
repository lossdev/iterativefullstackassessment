# Full Stack Assessment

## Purpose

This application is Kubernetes based. It deploys an Express backend server with a React frontend server. The frontend server takes in 
two sets of Lat/Long coordinates (an origin and destination); assuming that the coordinates are valid, the backend will process the 
distance and time between the two coordinates using the Google Maps Distance Matrix API. Any previously found routes will be displayed
in a table on the UI, and the routes will also be stored in a PostgreSQL server deployed in tandem with the backend and frontend. Each time
the frontend starts, it will make a call to the backend for initial routes already stored in the SQL server, then display those on the routes
table for the user.

## Installation / Environment

This application was developed and tested in a macOS 11 Big Sur environment, however, any modern macOS or Linux distribution should be able to
run the application. 

First, clone the repo:
```bash
git clone https://lossdev@bitbucket.org/lossdev/iterativefullstackassessment.git
```

You will need to install `minikube`, `docker`, and `helm`. 

*  `docker` is needed to containerize the application components. Dependencies on the host are not needed, as they are all in the container.
*  `minikube` sets up a simple Kubernetes cluster local to your computer. The docker images will be deployed onto this cluster.
*  `helm` is a packaging software used for Kubernetes based applications. A whole application with its many components, networking needs, etc can be stood up simultaneously with one helm command.

*** NOTE: Make sure that `minikube` is at least version `1.23.0` or higher! You can verify this by executing `minikube version` in a terminal window.

On macOS, Docker Desktop must be installed to provide `docker`. It can be found here: https://docs.docker.com/desktop/mac/install/. `minikube` and `helm` can both be
installed from the `brew` package manager at https://brew.sh

```bash
brew install minikube helm
``` 

Now enable the `storage-provisioner`, `default-storageclass`, and `ingress` minikube addons:

```bash
minikube addons enable storage-provisioner
minikube addons enable default-storageclass
minikube addons enable ingress
```

For better performance, it is recommended that you pull the `postgres:13` image before deploying the application - otherwise, the database will not deploy immediately with the frontend and backend, as Docker will need to pull the postgres image before being able to load postgres. To do so:

```bash
docker pull postgres:13
```

Then, start the minikube cluster:

```bash
minikube start --driver=docker
```

The `minikube` commands and `docker pull` can be done manually, or there is a script in the root directory of the git repo called `cluster-setup.sh` that can be
executed that execute those exact commands for you.

After this, instruct Docker to use the `minikube` cluster's Docker store, not your host's:

```bash
eval $(minikube docker-env)
```

This command will work only for the terminal window it was executed in - make sure you execute this command and the subsequent ones in the same window.

Finally, the frontend and backend containers will have to be built. You can do this manually:
```bash
cd frontend
docker build -t $(cat IMAGE) .
cd ../backend
docker build -t $(cat IMAGE) .
```

or through the `build-containers.sh` script also in the root directory which will execute those commands for you. 

One last step: a Google Maps API Key with the Javascript Distance Matrix enabled must be present in the `fullstack-deployment/values.yaml` file in the `mapsApiKey` field. It should look like this:

```yaml
mapsApiKey: abcd123edgh5678
```

## Deploying

In the root directory of the git repository, execute the following:
```bash
helm install deployment ./fullstack-deployment
minikube tunnel
```

On macOS, the `minikube tunnel` command will ask for your administrator password - this is used to set up a port forwarding scheme from the nginx ingress used in the
minikube cluster to `localhost` ports 8000 and 8080 on your host. On Linux, you may need to use `sudo minikube tunnel` for the same effect.

Once the tunnel is started, give the ingress about 30 seconds to a minute to fully establish the routes necessary to forward the application. Afterwards, visit
http://localhost:8080 on your host browser - the UI should be on your screen at this point.

## Contact / Questions

Contact me at: bmwilson.dev@gmail.com