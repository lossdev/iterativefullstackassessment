import React, { Component } from 'react';
import { createTheme, withStyles } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';
import { Table, TableBody, TableCell, TableHead, TableRow, Typography, Box, Button, TextField, responsiveFontSizes } from '@material-ui/core';
import axios from 'axios';
import '@fontsource/jetbrains-mono';

let theme = createTheme({
    palette: {
        type: 'dark',
    },
    typography: {
        allVariants: {
            color: "ivory"
       },
        fontFamily: "'JetBrains Mono', 'Segoe UI', Roboto, sans-serif"
    }
});
const RedTypography = withStyles({
    root: {
        color: "#EA383B",
    }
})(Typography)
theme = responsiveFontSizes(theme);

class App extends Component {
    state = {
        error: "",
        originLat: "",
        originLong: "",
        destLat: "",
        destLong: "",
        distance: "",
        time: "",
        rows: [],
    };

    componentDidMount = () => {
        axios.get('http://localhost:8000/getExistingData')
        .then(response => {
            let rows = this.state.rows;
            for (let i = 0; i < response.data.length; i++) {
                rows.push(this.createData(response.data[i].origlat, response.data[i].origlong, response.data[i].destlat, response.data[i].destlong, response.data[i].distance, response.data[i].duration));
                this.setState({rows: rows});
            }
        })
        .catch(error => {
            console.log(error.response.data.error);
            this.setState({error: error.response.data.error});
        })
    }

    handleOriginLatChange = (e) => {
        this.setState({originLat: e.target.value});
    }

    handleOriginLongChange = (e) => {
        this.setState({originLong: e.target.value});
    }

    handleDestLatChange = (e) => {
        this.setState({destLat: e.target.value});
    }

    handleDestLongChange = (e) => {
        this.setState({destLong: e.target.value});
    }

    createData = (oLat, oLong, dLat, dLong, dist, dur) => {
        return { oLat, oLong, dLat, dLong, dist, dur };
    }

    handleSubmit = () => {
        this.setState({error: ""});
        // error check
        if (this.state.originLat === "" || this.state.originLong === "" || this.state.destLat === "" || this.state.destLong === "") {
            this.setState({error: "Fields Cannot Be Empty, Try Again"});
            return;
        } else if (this.state.originLat < -90.0 || this.state.originLat > 90.0 || this.state.destLat < -90.0 || this.state.destLat > 90.0) {
            this.setState({error: "Latitude Cannot Be Less Than -90 or Greater Than 90"});
            return;
        } else if (this.state.originLong < -180.0 || this.state.originLong > 180.0 || this.state.destLong < -180.0 || this.state.destLong > 180.0) {
            this.setState({error: "Longitude Cannot Be Less Than -180 or Greater Than 180"});
            return;
        }
        axios.get('http://localhost:8000/travelTime?origLat=' + this.state.originLat + '&origLong=' + this.state.originLong + '&destLat=' + this.state.destLat + '&destLong=' + this.state.destLong)
        .then(response => {
            let distance = response.data.rows[0].elements[0].distance.text;
            let duration = response.data.rows[0].elements[0].duration.text;
            let rows = this.state.rows;
            rows.push(this.createData(this.state.originLat, this.state.originLong, this.state.destLat, this.state.destLong, distance, duration));
            this.setState({distance: distance});
            this.setState({time: duration});
            this.setState({rows: rows});
        })
        .catch(error => {
            console.log(error.response.data.error);
            this.setState({error: error.response.data.error});
        })
    }

    render = () => (
        <ThemeProvider theme={theme}>
            <Box display="flex" flexDirection="row">
                <Box display="flex" width="20%"></Box>
                <Box display="flex" flexDirection="column" width="60%">
                    <Box display="flex" width="100%" height="10vh"></Box>
                    <Box display="flex" justifyContent="center" alignItems="center" textAlign="center">
                        <Typography variant="h4">Google Maps Distance Calculator</Typography>
                    </Box>
                    <Box display="flex" width="100%" pt={5}>
                        <Box display="flex" width="50%" justifyContent="center" alignItems="center" textAlign="center" flexDirection="column">
                            <Typography variant="h6">Origin</Typography>
                            <Box display="flex" justifyContent="center" alignItems="center" textAlign="center" flexDirection="row" pt={3}>
                                <form noValidate autoComplete="off">
                                    <TextField id="lat-origin" label="Latitude" variant="outlined" margin="normal" value={this.state.originLat} onChange={this.handleOriginLatChange} />
                                    <TextField id="long-origin" label="Longitude" variant="outlined" margin="normal" value={this.state.originLong} onChange={this.handleOriginLongChange} />
                                </form>
                            </Box>
                        </Box>
                        <Box display="flex" width="50%" justifyContent="center" alignItems="center" textAlign="center" flexDirection="column">
                            <Typography variant="h6">Destination</Typography>
                            <Box display="flex" justifyContent="center" alignItems="center" textAlign="center" flexDirection="row" pt={3}>
                                <form noValidate autoComplete="off">
                                    <TextField id="lat-dest" label="Latitude" variant="outlined" margin="normal" value={this.state.destLat} onChange={this.handleDestLatChange} />
                                    <TextField id="long-dest" label="Longitude" variant="outlined" margin="normal" value={this.state.destLong} onChange={this.handleDestLongChange} />
                                </form>
                            </Box>
                        </Box>
                    </Box>
                    { this.state.error && <Box display="flex" width="100%" justifyContent="center" alignItems="center" textAlign="center" pt={3}>
                            <RedTypography variant="body1">{this.state.error}</RedTypography>
                    </Box>}
                    <Box display="flex" width="100%" flexDirection="row" pt={5}>
                        <Box display="flex" width="38%"></Box>
                        <Box display="flex" width="24%" justifyContent="center" alignItems="center" textAlign="center">
                            <Button variant="contained" onClick={this.handleSubmit}>Run Calculation</Button>
                        </Box>
                        <Box display="flex" width="38%"></Box>
                    </Box>
                    <Box display="flex" width="100%" pt={5}>
                        <Table size="small">
                            <TableHead>
                                <TableRow>
                                    <TableCell>Orig. Lat</TableCell>
                                    <TableCell align="right">Orig. Long</TableCell>
                                    <TableCell align="right">Dest. Lat</TableCell>
                                    <TableCell align="right">Dest. Long</TableCell>
                                    <TableCell align="right">Distance&nbsp;(mi)</TableCell>
                                    <TableCell align="right">Duration</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {this.state.rows.map((row) => (
                                    <TableRow key={row.oLat}>
                                        <TableCell component="th" scope="row">{row.oLat}</TableCell>
                                        <TableCell align="right">{row.oLong}</TableCell>
                                        <TableCell align="right">{row.dLat}</TableCell>
                                        <TableCell align="right">{row.dLong}</TableCell>
                                        <TableCell align="right">{row.dist}</TableCell>
                                        <TableCell align="right">{row.dur}</TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                    </Table>
                    </Box>
                </Box>
                <Box display="flex" width="20%"></Box>
            </Box>
        </ThemeProvider>
    );
}

export default App;