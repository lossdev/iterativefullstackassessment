const { Client } = require('pg');
var express = require('express');
var cors = require('cors');
var nodeDistanceMatrix = require('node-distance-matrix');

var app = express();
app.use(cors());
const apiKey = process.env.API_KEY;
const client = new Client({
    user: 'postgres',
    host: 'postgres',
    database: 'postgres',
    password: process.env.POSTGRES_PASSWORD,
    port: 5432,
});
try  {
    client.connect();
} catch (error) {
    console.log(error);
}
client.query(`CREATE TABLE IF NOT EXISTS LocationData (
    OrigLat VARCHAR(10) NOT NULL, OrigLong VARCHAR(10) NOT NULL,
    DestLat VARCHAR(10) NOT NULL, DestLong VARCHAR(10) NOT NULL,
    Distance VARCHAR(15) NOT NULL, Duration VARCHAR(20) NOT NULL);
`, (err, res) => {
    console.log(err, res);
});

app.get('/travelTime', function (req, res, next) {
    let origLat = req.query.origLat;
    let origLong = req.query.origLong;
    let destLat = req.query.destLat;
    let destLong = req.query.destLong;

    const distance = nodeDistanceMatrix.getDistanceMatrix(apiKey, origLat + ',' + origLong, destLat + ',' + destLong);
    distance.then(response => { res.send(response.data); addToDB(origLat, origLong, destLat, destLong, response); });
});

app.get('/getExistingData', function (req, res, next) {
    client.query("SELECT * FROM LocationData", (err, resp) => {
        if (err) {
            console.log(err.stack);
            res.send(err.stack);
        } else {
            res.send(resp.rows);
        }
    })
})

app.listen(8000, function () {
    console.log('Backend listening on port :8000');
});

function addToDB(origLat, origLong, destLat, destLong, resp) {
    let distance = resp.data.rows[0].elements[0].distance.text;
    let duration = resp.data.rows[0].elements[0].duration.text;
    const query = {
        text: 'INSERT INTO LocationData(OrigLat, OrigLong, DestLat, DestLong, Distance, Duration) VALUES ($1, $2, $3, $4, $5, $6)',
        values: [origLat, origLong, destLat, destLong, distance, duration],
    }
    client
        .query(query)
        .then(res => console.log(res.rows[0]))
        .catch(e => console.log(e.stack));
}